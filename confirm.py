#!/usr/bin/python
import cgi
import cgitb
import re
import copy
import pickle
import hmac
import base64
import lxml.html

requiredFields = ["Email", "Church", "LastName", "Address", "CityStateZip", "NeedRide", "GiveRide"]
missingFields = []
picklepw = b"My name is Ming-Yee."
hmacpw = b"I am a student."

def is_valid(dict, count):
	# TODO: If adversary manually submits bad form via JS command, etc.
	'''global missingFields
	missingFields = [x for x in requiredFields if x not in dict]
	if len(missingFields) > 0:
		return False
	if dict["NeedRide"] in ["T", "F", "B"] and "NeedRideCity" not in dict:
		return False
	if dict["GiveRide"] in ["T", "F", "B"] and ("Spaces" not in dict or "GiveRideCity" not in dict):
		return False
	if "FirstName[0]" not in dict or "Age[0]" not in dict or "Language[0]" not in dict or "Housing[0]" not in dict or\
		dict["Housing[0]"] is "C" and "Days[0]" not in dict:
		return False
	for x in range(1, count+1):
		if len({"FirstName["+str(x)+"]" in dict, "Age["+str(x)+"]" in dict, "Language["+str(x)+"]" in dict, "Housing["+str(x)+"]" in dict}) > 1 or\
			"Housing["+str(x)+"]" in dict and dict["Housing["+str(x)+"]"] is "C" and "Days["+str(x)+"]" not in dict:
			return False'''
	return True

def valid_form(d, count):
	## Confirmation page
	a = open('ConfirmationPage.html','r').read()
	tree = lxml.html.fromstring(a)
	
	# LastName
	tree.xpath('.//*[@id="LastName"]')[0].text = d['LastName'] + " family"
	
	# Registrants
	r = tree.xpath('.//*[@class="pinned-row"]')[0]
	for i in range(count):
		r.getparent().insert(1, copy.deepcopy(r))
	r = tree.xpath('.//*[@class="scrollable-row"]')[0]
	for i in range(count):
		r.getparent().insert(1, copy.deepcopy(r))

	for i in range(count+1):
		r = tree.xpath('.//*[@class="pinned-row"]')[i].findall('td')[0].text = d['FirstName[%d]' %i]
		r = tree.xpath('.//*[@class="pinned-row"]')[i].findall('td')[1].text = "$" + d['Cost[%d]' %i]
		r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[0].text = d['Age[%d]' %i]
		r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[1].text = d['Sex[%d]' %i]
		c = {'E': 'English', 'M': 'Mandarin', 'C': 'Cantonese'}
		r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[2].text = c[d['Language[%d]' %i]]
		c = {
			''  : '',
			'CC': 'Cantonese Couples I, II, III; Parenting',
			'AC': 'Agape Couples Fellowship',
			'CN': 'Canaan Fellowship',
			'GB': 'Greenbelt Fellowship',
			'CW': 'Cantonese Women&rsquo;s Group',
			'MC': 'Mandarin Couples I, II; Parenting',
			'MW': 'Mandarin Women&rsquo;s Group',
			'ME': 'Mandarin Elderly',
			'YA': 'English Young Adult',
			'PT': 'Parent&ndash;Teen Fellowship ',
			'EM': 'English Men&rsquo;s Group',
			'RJ': 'Rejoice Fellowship',
			'CB': 'CBCF Couples Fellowship',
			'BF': 'Berith Fellowship',
			'RF': 'Roots Fellowship',
			'OT': 'Other Fellowship'
		}
		r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[3].text = c[d.get('Fellowship[%d]' %i, '')]
		c = {'D': 'Dorm', 'A': 'Apartment'}
		if d['Housing[%d]' %i] is 'C':
			r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[4].text = 'Commute (%s)' %d['Days[%d]' %i]
		else:
			r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[4].text = c[d['Housing[%d]' %i]]
		r = tree.xpath('.//*[@class="scrollable-row"]')[i].findall('td')[5].text = d['Student[%d]' %i]
	
	# Delete empty spouse
	
	# Affiliation
	if d["Church"] is not "O":
		tree.xpath('.//*[@id="Affiliation"]')[0].text = "You attend CBC%s." %d["Church"]
	else:
		tree.xpath('.//*[@id="Affiliation"]')[0].text = "You do not attend CBCF or CBCCP."
	
	# Address
	tree.xpath('.//*[@id="Address"]')[0].text = d.get('Address', '[unlisted]')
	
	# CityStateZip
	tree.xpath('.//*[@id="CityStateZip"]')[0].text = d.get('CityStateZip', '[unlisted]')
	
	# HomePhone
	tree.xpath('.//*[@id="HomePhone"]')[0].text = d.get('HomePhone', '[unlisted]') + " (H)"
	
	# CellPhone
	tree.xpath('.//*[@id="CellPhone"]')[0].text = d.get('CellPhone', '[unlisted]') + " (C)"
	
	# Email
	tree.xpath('.//*[@id="Email"]')[0].text = d['Email']
	
	# SpecialRequests
	tree.xpath('.//*[@id="SpecialRequests"]')[0].text = d.get('SpecialRequests', 'No special requests.')
	
	# Transportation
	t = d['NeedRide'] + d['GiveRide']
	d['NeedRideCity'] = d.get('NeedRideCity', '')
	d['GiveRideCity'] = d.get('GiveRideCity', '')
	d['Spaces'] = d.get('Spaces', '')
	c = {
		'TF': 'You need a ride to retreat from %s, but can give a ride from retreat to %s, for %s people.'
		       %(d['NeedRideCity'], d['GiveRideCity'], d['Spaces']),
		'TN': 'You need a ride to retreat from %s.' %d['NeedRideCity'],
		'FT': 'You can give a ride to retreat from %s, for %s people, but need a ride from retreat to %s.'
		       %(d['GiveRideCity'], d['Spaces'], d['NeedRideCity']),
		'FN': 'You need a ride from retreat to %s.' %d['NeedRideCity'],
		'BN': 'You need rides both to and from retreat from and to %s.' %d['NeedRideCity'],
		'NT': 'You can give a ride to retreat from %s, for %s people.' %(d['GiveRideCity'], d['Spaces']),
		'NF': 'You can give a ride from retreat to %s, for %s people.' %(d['GiveRideCity'], d['Spaces']),
		'NB': 'You can give rides both to and from retreat from and to %s, for %s people.' %(d['GiveRideCity'], d['Spaces']),
		'NN': 'You neither need a ride nor can give a ride to or from retreat.'
	}
	tree.xpath('.//*[@id="Transportation"]')[0].text = c[t]

	# Meals
	if 'C' not in [d['Housing[%d]' %i] for i in range(count)]:
		tree.xpath('.//*[@id="Meals"]')[0].text = "Meals are fully provided by housing option."
	else:
		d['Meal'] = [x.value for x in d.get('Meal', [])]
		counts = {'B':0, 'L':0, 'D':0}
		for x in d['Meal']:
			counts[x[0]] += 1		
		tree.xpath('.//*[@id="Meals"]')[0].text = \
			"You have signed up for " + \
			'%d breakfast' %counts['B'] + ('s' if counts['B'] != 1 else '') + ', ' + \
			'%d lunch' %counts['L'] + ('es' if counts['B'] != 1 else '') + ', and ' + \
			'%d dinner' %counts['D'] + ('s' if counts['B'] != 1 else '') + ' for commuters.'
	
	# TotalCost
	tree.xpath('.//*[@id="TotalCost"]')[0].text = "$" + d['TotalCost']
	
	# Dump data and hash into form
	dump = pickle.dumps(d)
	pw1  = hmac.new(picklepw, dump).hexdigest()
	pw2  = hmac.new(hmacpw, pw1.encode()).hexdigest()
	tree.xpath('.//*[@name="Dump"]')[0].attrib['value'] = base64.b64encode(dump)
	tree.xpath('.//*[@name="Hash1"]')[0].attrib['value'] = pw1
	tree.xpath('.//*[@name="Hash2"]')[0].attrib['value'] = pw2
	
	# Print page
	print(lxml.html.tostring(tree).decode())

def invalid_form():
	pass

def main():
	cgitb.enable(display = 0, logdir = "logs")
	form = cgi.FieldStorage()
	d = {k : getattr(form[k], 'value', form[k]) for k in form.keys()}
	maxIndex = max([int(k[10:k.find("]")]) for k in form.keys() if k.find("FirstName") == 0])
	d['Count'] = maxIndex+1
	
	print("Content-type: text/html; charset=UTF-8\nContent-Language: en\n\n")
	if is_valid(d, maxIndex):
		valid_form(d, maxIndex)
	else:
		invalid_form()
	
if __name__ == '__main__':
	main()
