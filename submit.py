#!/usr/bin/python

import cgi
import cgitb
import re
import lxml.html
import pickle
import hmac
import base64
import random
from confirm import hmacpw

def main():
	cgitb.enable(display = 0, logdir = "log")
	form = cgi.FieldStorage()
	d = {k : getattr(form[k], 'value', form[k]) for k in form.keys()}
	
	print("Content-type: text/html; charset=UTF-8\nContent-Language: en\n\n")
	
	hmac1 = d['Hash1']
	hmac2 = d['Hash2']
	if hmac2 == hmac.new(hmacpw, hmac1.encode()).hexdigest():
		dump = pickle.loads(base64.b64decode(d['Dump']))
		rhash = "%016x" %random.getrandbits(64)
		
		# Write data dump to file
		f = open('Registrations/'+rhash, 'w')
		f.write(str(dump))
		f.flush()
		f.close()
		
		# Write submission message
		print("Thank you for registering.")
		print("Your registration confirmation ID is %s." %rhash)
		print("Please retain this for your records.")
	else:
		print("ATTACKER!")
	
if __name__ == '__main__':
	main()
