var currentTabIndex = 0;
var submitLock = false;
var deadline = new Date("April 27" + " " + (new Date()).getFullYear()); // FIXME: make changeable and not dependent on year in this way
var today = new Date();

function addHouseholdMember() {
	var count = $('ul#RegistrantTabs > li').length-1;
	
	// Add form
	$('#MemberTemplate').clone().insertAfter('#MemberTemplate');
	$('#MemberTemplate:first').attr('id', 'Member'+count);
	$('#Member'+count+' [name*=FirstName]').bind("propertychange change click keyup input paste",
		function(event) {updateTab(this, count);}
	);
	$('#Member'+count+' [id*=Delete]').click(function() {
		deleteTab(count);
	});
	$('#Member'+count+' *').each(function(i, obj) {
		if (typeof $(obj).attr('for') !== 'undefined')
			$(obj).attr('for', $(obj).attr('for').replace('[i]', '['+count+']'));
		if (typeof $(obj).attr('name') !== 'undefined')
			$(obj).attr('name', $(obj).attr('name').replace('[i]', '['+count+']'));
		if (typeof $(obj).attr('id') !== 'undefined')
			$(obj).attr('id', $(obj).attr('id').replace('[i]', '['+count+']'));
	});
	$('#Member'+count+' [name*=Age]').change(function() {calculateCost()});
	$('#Member'+count+' [name*=Housing]').change(function() {commute(); calculateCost()});
	$('#Member'+count+' [name*=Days]').change(function() {calculateCost()});
	$('#Member'+count+' [name*=Student]').change(function() {calculateCost()});
	
	// Add tab
	var newTab = $('ul#RegistrantTabs > li:first').clone();
	newTab.removeClass('active');
	newTab.removeClass('missing');
	newTab.attr('id', 'Tab'+count);
	newTab.attr('onclick', 'changeTab('+count+')');
	newTab.children()[0].text = 'Member '+(count+1);
	newTab.insertBefore('ul#RegistrantTabs > li:last');
	processMeals();
}
function changeTab(i) {
	$('#Member'+i).removeClass('hidden');
	if (i != currentTabIndex)
		$('#Member'+currentTabIndex).addClass('hidden');
	currentTabIndex = i;
}
function updateTab(self, i) {
	var input = $(self).val();
	if(input === undefined || input.length > 0)
		$('#Tab'+i+'>a').text(input);
	else switch (i) {
		case 0:	$('#Tab'+i+'>a').text('You'); break;
		case 1: $('#Tab'+i+'>a').text('Spouse'); break;
		default: $('#Tab'+i+'>a').text('Member '+(i+1));
	}
}
function toggleSex(self, i) {
	$('select[name=\'Sex['+(1-i)+']\']').val($(self).val() == 'M' ?   'F' : 'M');
}
function docInit() {
	makeDocTitle();
	processMeals();
	showSpaces();
	
	$('#Member0 [name*=FirstName]').bind("propertychange change click keyup input paste",
		function(event) {updateTab(this, 0);}
	);
	$('#Member1 [name*=FirstName]').bind("propertychange change click keyup input paste",
		function(event) {updateTab(this, 1);}
	);
	$('[id="Delete[1]"]').click(function() {
		$('#Member1 [name*=FirstName]').val('');
		$('#Member1 [name*=FirstName]').click();
		$('#Member1 [name*=Age]').val('');
		$('#Member1 [name*=Language]').val('');
		$('#Member1 [name*=Fellowship]').val('');
		$('#Member1 [name*=Housing]').val('');
		$('#Member1 [name*=Student]').val('N');
		$('#Member1 [name*=Cost]').val('');
	});
	
	// Set up cost updating
	$('[name=Church]').click(function() {calculateCost()});
	$('[name*=Age]').change(function() {calculateCost()});
	$('[name*=Housing]').change(function() {commute(); calculateCost()});
	$('[name*=Days]').change(function() {calculateCost()});
	$('[name*=Student]').change(function() {calculateCost()});
	$('#MealChecker').click(function() {toggleMealChecker();calculateCost()});
	$('[name=Meal]').click(function() {toggleMealChecker();calculateCost()});
	
	// Set up validation
	$('[name*=Age]').blur(function(event) {validateAge(event)});
	$('[name=HomePhone]').blur(function(event) {validatePhones(event)});
	$('[name=CellPhone]').blur(function(event) {validatePhones(event)});
	$('[name=Email]').blur(function(event) {validateEmail(event)});
	$('[name$=Ride]').change(function(event) {validateTransportation(event)});
	$('[name=Spaces]').blur(function(event) {validateSpaces(event)});
	
	// Set up form submission and reset buttons
	$('#Submit').click(function() {if (checkForm()) document.forms[0].submit(); })
	$('#Reset').click(function() { resetForm(); });
}
function makeDocTitle() {
	var d = memorialDay();
	$('#subtitle').html("Chinese Bible Church of College Park/Fairfax");
	$('#title').html(toOrdinal(d.getFullYear()-2007)+" Annual Messiah College Retreat");
	$('#date').html("(May "+(d.getDate()-2)+"&ndash;"+d.getDate()+", "+d.getFullYear()+")");
}
function memorialDay() {
	var y = new Date().getFullYear();
	var d = new Date(y, 4, 25 + (8-new Date(y, 4, 25).getDay())%7);
	if((new Date()) > d)
		d = new Date(y+1, 4, 25 + (8-new Date(y+1, 4, 25).getDay())%7);
	return d;
}
function toOrdinal(s) { //convert ordinals up to 99th
	var dg = ['\b\bieth','first','second','third','fourth','fifth','sixth','seventh','eighth','ninth'];
	var tn = ['tenth','eleventh','twelveth','thirteenth','fourteenth','fifteenth','sixteenth','seventeenth','eighteenth','nineteenth'];
	var tw = [,,'twenty-','thirty-','forty-','fifty-', 'sixty-','seventy-','eighty-','ninety-'];
	var n = s.toString().split('');
	var str = '';
	
	if(n.length == 2 && n[n.length-2] == '1')
		return tn[n[1]].replace(/^\w/, function($0) { return $0.toUpperCase(); });
	for (var i = 0; i < n.length; i++)
		str += (n.length-i-1 == 1)? tw[n[i]] : dg[n[i]];
	return str.replace(/^\w/, function($0) { return $0.toUpperCase(); });
}
function commute() {
	processMeals();
}
function processMeals() {
	var count = $('ul#RegistrantTabs > li').length-1;
	var commuter = false;
	
	for(var i = 0; i < count; i++)
		if($('[name="Housing['+i+']"]').val() == "C")
		{
			commuter = true;
			$('*[name="Days['+i+']"]').removeClass('hidden');
		}
		else
		{
			$('[name="Days['+i+']"]').addClass('hidden');
			$('[name="Days['+i+']"]').val('');
		}
	for(var i = 0; i < 7; i++)
	{
		$('*[name=Meal]')[i].readOnly = !commuter;
		$('*[name=Meal]')[i].onclick = function() {calculateCost();return commuter};
	}
}
function toggleMealChecker() {
	var allChecked = true;
	var commuter = false;
	var count = $('ul#RegistrantTabs > li').length-1;
	
	for(var i = 0; i < count; i++)
		if(commuter |= $('[name="Housing['+i+']"]').val() == "C")
			break;
	if(!commuter)
		return;
	
	var m = $('[name=Meal]');
	for(var i = 0; i < 7; i++)
		allChecked &= m[i].checked;
	
	$('#MealChecker').click(function() {
		var a = m;
		if(!m[0].readOnly)
			for(var i = 0;i < a.length;i++)
				a[i].checked = !(allChecked == 1);
		calculateCost(); toggleMealChecker()
	});
	$('#MealChecker').html(allChecked ?   'Check none' : 'Check all');
}
function showSpaces() {
	if($.inArray($('[name=NeedRide]').val(), ['T', 'F', 'B']) >= 0)
		$('#NeedRideInfo').removeClass('hidden');
	else
		$('#NeedRideInfo').addClass('hidden');
	if($.inArray($('[name=GiveRide]').val(), ['T', 'F', 'B']) >= 0)
		$('#GiveRideInfo').removeClass('hidden');
	else
		$('#GiveRideInfo').addClass('hidden');
}
function errorNotify(msg) {
	if (submitLock)	return false;
	
	$.notify({
		icon: 'glyphicon glyphicon-danger-sign',
		message: msg
	}, {
		type: 'danger',
		placement: {align: 'center'},
		delay: 3000
	});
	return false;
}
function resetForm() {
//	location.reload(true);
	location.reload(false);
}
function isNotMissing(jqobj, i) {
	if (i == 1 && spouseIsDefault() || i == $('ul#RegistrantTabs > li').length-1)
		return true;
	if (jqobj.val() == '') {
		jqobj.addClass('missing');
		if (typeof i != undefined)
			$('#Tab'+i).addClass('missing');
		return false;
	}
	return true;
}
function spouseIsDefault() {
	return $('#Member1 [name*=FirstName]').val() == ''
		&& $('#Member1 [name*=Age]').val() == ''
		&& $('#Member1 [name*=Language]').val() == ''
		&& $('#Member1 [name*=Fellowship]').val() == ''
		&& $('#Member1 [name*=Housing]').val() == ''
		&& $('#Member1 [name*=Student]').val() == 'N'
		&& $('#Member1 [name*=Cost]').val() == '';
}
function checkForm() {
	var valid = submitLock = true;
	$('.missing').removeClass('missing');
	valid &= validateForm();
	
	// Check for empty required fields
	if (typeof $('[name=Church]:checked').val() == 'undefined') {
		valid = false;
		$('#ChurchButtons').addClass('missing');
	}
	valid &= isNotMissing($('[name=LastName]'));
	$('[name*=FirstName]').each(function(i, obj) { valid &= isNotMissing($(obj), i); })
	$('[name*=Housing]').each(function(i, obj) { valid &= isNotMissing($(obj), i); })
	valid &= isNotMissing($('[name=Email]'));
	valid &= isNotMissing($('[name=NeedRide]'));
	valid &= isNotMissing($('[name=GiveRide]'));
	if ($('[name=NeedRideCity]').is(':visible'))
		valid &= isNotMissing($('[name=NeedRideCity]'));
	if ($('[name=GiveRideCity]').is(':visible')) {
		valid &= isNotMissing($('[name=Spaces]'));
		valid &= isNotMissing($('[name=GiveRideCity]'));
	}
	
	submitLock = false;
	if (valid != 1)
		errorNotify('Some entries were invalid: please review the highlighted fields.');
	return valid == 1;
}
function validateForm() {
	var valid = true;
	valid &= validateAllAges();
//	valid &= validateZip();
	valid &= validateAllPhones();
	valid &= validateEmail()
	if (!validateTransportation()) {
		$('[name$=Ride]').addClass('missing');
		valid = false;
	}
	valid &= validateSpaces();
	return valid == 1;
}
function validateAllAges() {
	var valid = true;
	$('[name*=Age]').each(function(i, obj) {
		if (!(i == 1 && spouseIsDefault() || i == $('ul#RegistrantTabs > li').length-1)
		    && !validateAge({target: obj})) {
			$(obj).addClass('missing');
			valid = false;
		}
	});
	return valid;
}
function validateAge(event) {
	var field = $(event.target);
	var age = Number(field.val());
	
	if(!isNaN(age) && !isNaN(parseInt(age)))
		if(age <= 0)
			return errorNotify('Age must be a nonnegative number');
		else
			return true;
	
	// field.val('');
	return errorNotify('Invalid age: only arabic numerals are allowed.');
}
function validateZip() {
	return "";
}
function validateAllPhones() {
	$('[name$=Phone]').each( function(_, obj) {
		if (!validatePhones({target: obj}))
			return false;
	});
	return true;
}
function validatePhones(event) {
	var field = $(event.target);
	var re = /^([(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}|[\s]{0,})$/;
	
	if (re.test(field.val()))
		return true;
	
	// field.val('');
	return errorNotify('Invalid phone number: only 10-digit US numbers allowed.');
}
function validateEmail(event) {
	var email = $('[name=Email]').val();
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	if (email == "" || re.test(email))
		return true;
	
	// field.val('');
	return errorNotify('Invalid email address.');
}
function validateTransportation(event) {
	var valid = true;
	var giveIndex = $.inArray($('[name=GiveRide]').val(), ['x', 'T', 'F', 'B']);
	var needIndex = $.inArray($('[name=NeedRide]').val(), ['x', 'T', 'F', 'B']);
	
	if(giveIndex > 0 && needIndex > 0 && (giveIndex & needIndex) > 0)
		valid = errorNotify('You cannot both need and give a ride.');
	
	return valid;
}
function validateSpaces(event) {
	if (!$('[name=Spaces]').is(':visible'))
		return true;
	
	var spaces = Number($('[name=Spaces]').val());
	
	if(!isNaN(spaces) && !isNaN(parseInt(spaces)) && spaces <= 0)
		return errorNotify('Number of spaces must be a positive number');
	if($.inArray($('[name=GiveRide]').val(), ['T', 'F', 'B']) >= 0 && isNaN(spaces))
		return errorNotify('Invalid number of rides: only arabic numerals are allowed.');
	
	return true;
}
function deleteTab(t) {
	var count = $('ul#RegistrantTabs > li').length-1;
	
	$('#Member'+t).remove();
	$('#Tab'+t).remove();
	for (t++; t < count; t++) (function(i) {
		$('#Tab'+i).attr('onclick', 'changeTab('+(i-1)+')');
		$('#Tab'+i).attr('id', 'Tab'+(i-1));
		$('#Member'+i+' [for$="['+i+']"').each(function (_, obj) {
			$(obj).attr('for', $(obj).attr('for').replace('['+i+']', '['+(i-1)+']'));
		});
		$('#Member'+i+' [name$="['+i+']"').each(function (_, obj) {
			$(obj).attr('name', $(obj).attr('name').replace('['+i+']', '['+(i-1)+']'));
		});
		$('#Member'+i+' [id$="['+i+']"').each(function (_, obj) {
			$(obj).attr('id', $(obj).attr('id').replace('['+i+']', '['+(i-1)+']'));
		});
		$('#Member'+i+' [name*=FirstName]').unbind("propertychange change click keyup input paste");
		$('#Member'+i+' [name*=FirstName]').bind("propertychange change click keyup input paste",
			function(event) { updateTab(this, i-1); }
		);
		$('#Member'+i+' [name*=FirstName]').click();
		$('#Member'+i+' [id*=Delete]').unbind('click');
		$('#Member'+i+' [id*=Delete]').click(function() { console.log(i-1); deleteTab(i-1); });
		$('#Member'+i).attr('id', 'Member'+(i-1));
	})(t)
	processMeals();
	
	// Move focus down a tab, if necessary
	if (currentTabIndex == count-1)
		currentTabIndex--;
	$('#Tab'+currentTabIndex+'>a').click();
}
function confirmDocInit() {
	$('input[type=checkbox]').click(function () {
		if ($('input[type=checkbox]').length == $('input:checked').length){
			$('#Submit1').removeClass('disabled');
			$('#Submit2').removeClass('disabled');
			$('#Submit1').click(function () {
				document.forms[0].submit();
			});
			$('#Submit2').click(function () {
				document.forms[0].submit();
			});
		}
		else {
			$('#Submit1').addClass('disabled');
			$('#Submit2').addClass('disabled');
			$('#Submit1').unbind('click');
			$('#Submit2').unbind('click');
		}
	})
}
function pricingDocInit() {
	$('#RelativeDate').text(today < deadline ?   "before" : "after");
	$('#EarlyBirdDeadline').text(deadline.toDateString());
	$('#InEffect').text(today < deadline ?  "still" : "no longer");
}
