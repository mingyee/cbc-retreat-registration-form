var CBC_cost_table = [
	{Ranges:[4,8,64,100,"Student"], _blank:["","","","",""], A:[0,110,150,110,'--'], D:[0, 90,125,125,110]},
	{Ranges:[4,8,64,100,"Student"], _blank:["","","","",""], A:[0,125,160,125,'--'], D:[0,115,145,145,125]}
];
var other_cost_table = [
	{Ranges:[4,8,64,100,"Student"], _blank:["","","","",""], A:[0,115,155,115,'--'], D:[0, 95,130,130,115]},
	{Ranges:[4,8,64,100,"Student"], _blank:["","","","",""], A:[0,130,165,130,'--'], D:[0,120,150,150,130]}
];
var commute_cost_table = [
	{Ranges:[4,8,100], C:[0,10,10]},
	{Ranges:[4,8,100], C:[0,20,20]}
];
var meal_cost_table = {Ranges:[4,8,100], B:[0,4.90,6.35], L:[0,8.80,11.75], D:[0,9.70,13.15]};

function calculateCost()
{
	var lateIndex = today < deadline ?   0 : 1;
	var CBC_cost = CBC_cost_table[lateIndex];
	var other_cost = other_cost_table[lateIndex];
	var commute_cost = commute_cost_table[lateIndex];
	var meal_cost = meal_cost_table;
	
	var cost = {Ranges:[1000],_blank:[""],A:[""],D:[""],C:[""]}, meals = {B:0,L:0,D:0};
	if($('[name="Church"]')[0].checked || $('[name="Church"]')[1].checked)
		cost = CBC_cost;
	else if($('[name="Church"]')[2].checked)
		cost = other_cost;
	
	var m = $('[name="Meal"]');
	for(var i = 0; i < m.length; i++)
		if(m[i].checked)
			meals[m[i].value.substring(0,1)]++;
	
	var sum = 0;
	var count = $('ul#RegistrantTabs > li').length-1;
	for(var i = 0; i < count; i++)
	{
		var age = $('[name="Age['+i+']"]')[0].value, housing = $('[name="Housing['+i+']"]')[0].value;
		if(age == "" || housing == "")
		{
			$('[name="Cost['+i+']"]')[0].value = '';
			continue;
		}
		
		if(housing != "C")
		{
			for(var j = 0; j < cost["Ranges"].length && age > cost["Ranges"][j]; j++);
			j = ($('[name="Student['+i+']"]')[0].value == 'Y' && cost[housing][cost[housing].length-1]<cost[housing][j]) ?   cost[housing].length-1 : j;
			housingCost = cost[housing][j];
		}
		else
		{
			for(var j = 0; j < commute_cost["Ranges"].length && age > commute_cost["Ranges"][j]; j++);
			housingCost = ($('[name="Days['+i+']"]')[0].value == "") ?   "" : commute_cost["C"][j]*$('[name="Days['+i+']"]')[0].value;
		}
		
		for(var k = 0; k < meal_cost["Ranges"].length && age > meal_cost["Ranges"][k]; k++);
		var mealCost = meals["B"]*meal_cost["B"][k] + meals["L"]*meal_cost["L"][k] + meals["D"]*meal_cost["D"][k];
		mealCost = (housing != "C" || housingCost == "")? "" : Math.round(mealCost*100)/100;
		
		totalCost = housingCost + mealCost;
		document.getElementsByName("Cost["+i+"]")[0].value = (String(totalCost) == "")? "" : parseFloat(totalCost).toFixed(2);
		sum += (document.getElementsByName("Cost["+i+"]")[0].value=="")? 0 : parseFloat(document.getElementsByName("Cost["+i+"]")[0].value);
	}
	document.getElementsByName("TotalCost")[0].value = (sum==0)? "" : (Math.round(sum*100)/100).toFixed(2);
}
